#!/bin/bash
# command line for service
case $1 in
"nginx" | "php-fpm")
  docker exec -it webserver service "$@"
  ;;
"supervisorctl")
  docker exec -it webserver "$@"
  ;;
"mariadb" | "redis" | "mongodb" | "cloud9")
  if [ $2 != "status" ]; then
    docker $2 $1
  else
    docker ps -f name=$1
  fi
  ;;
*)
  docker ps -a -f name=webserver -f name=redis  -f name=mariadb -f name=mongodb -f name=cloud9
esac

