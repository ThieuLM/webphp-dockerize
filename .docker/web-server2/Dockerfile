FROM centos:centos7

LABEL maintainer="ThieuLM <luongminhthieu@gmail.com>"

## centos/systemd
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

VOLUME [ "/sys/fs/cgroup" ]

## update
RUN yum update -y && \
	yum install -y epel-release && \
	yum -y install initscripts

## nginx
RUN yum install -y nginx; yum clean all

## update repo php73
RUN yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
	yum install -y yum-utils && \
	yum-config-manager --enable remi-php73 && \
	yum -y update

# Install dependencies
RUN yum install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl

## install php
RUN yum install -y php-fpm php-xml php-cli php-bcmath php-dba php-gd php-intl php-mbstring php-mysql php-mysqld php-pdo php-soap php-pecl-apcu php-pecl-imagick php-pear php-pecl-redis5 php-pecl-mongodb php-pecl-zip php-pecl-xdebug

RUN mkdir -p /run/php-fpm && \
    chown nginx:nginx /run/php-fpm

RUN mkdir -p /var/lib/php/session && \
	chown nginx:nginx /var/lib/php/session

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install nodejs and npm
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash - && yum install -y nodejs

# Install yarn
RUN curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo && \
    rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg && \
    yum install -y yarn

# Installing supervisor
RUN yum -y install supervisor; yum clean all

COPY ./conf/nginx-conf.d/default.conf_example /etc/nginx/conf.d/default.conf

RUN systemctl enable php-fpm.service
RUN	systemctl enable nginx.service
RUN systemctl enable supervisord.service

RUN yum clean all

EXPOSE 80 443

VOLUME /etc

CMD [ "/usr/sbin/init" ]
