##Package for webserver php run by docker-compose v2

- server nginx + php 7.2: base from image centos7
- mariadb: base from image mariadb:10.2
- mongodb: base from image mongo:3.6
- redis: base from image redis:5.0.6
- cloud9 IDE: base from image linuxserver/cloud9

#### Requirements:
 1. Install docker
 2. Install docker-compose

#### Config .env
- Copy .docker/.env_example to .docker/.env
```bash
cp .docker/.env_example .docker/.env
```
- Edit .docker/.env and save it

#### Config nginx, php-fpm
Folder .docker/web-server2/conf include config which will be mount on server, Copy file from template or edit file if need
- .docker/web-server2/conf/nginx-conf.d : ~ /etc/nginx/conf.d : folder config vhost for nginx
- .docker/web-server2/conf/supervisord.d : ~ /etc/supervisord.d : folder config for supervisor
- .docker/web-server2/conf/php-fpm.d : ~ /etc/php-fpm.d : folder config for php-fpm 
- .docker/web-server2/conf/php/php.ini : ~ /etc/php.ini

#### Config Xdebug
Xdebug default enable, if need change, edit file .docker/web-server2/conf/php/php.ini and rebuild docker-compose

Config default xdebug: (*)
````
xdebug.remote_enable=1
xdebug.remote_port=9000
xdebug.remote_autostart=0
xdebug.remote_connect_back=0
xdebug.remote_handler=dbgp
xdebug.profiler_enable=0
xdebug.remote_log=/tmp/xdebug.log
xdebug.idekey=idekey-xdebug
xdebug.remote_host=host.docker.internal
xdebug.auto_trace=0
````

Config IDE IntelliJ:
- Preferences > Languages & Frameworks > PHP > Debug : Change port to 9000
- Preferences > Languages & Frameworks > PHP > Debug > DBGp Proxy: Fill config from config xdebug (*)
- Preferences > Languages & Frameworks > PHP > Server: Add/Edit server and path mapping

If have error breakpoint: Debug session was finished without being paused, need check mapping Server, View more: https://stackoverflow.com/questions/53518174/xdebug-breakpoints-not-working-in-laravel

#### Usage

Build and start: 

```bash
cd .docker && docker-compose up --build -d
```

Stop: 

```bash
cd .docker && docker-compose stop
```

Folder mount data: .docker/.data


####Access Url

- Cloud9 IDE: http://localhost:PORT_IDE , example: http://localhost:18000
- Web server: http://localhost:PORT_HTTP , example: http://localhost:180

####Check status/start/stop/restart service
- {service} = nginx, php-fpm, mariadb, redis, mongodb, cloud9
```
.docker/service.sh <service> status
.docker/service.sh <service> start
.docker/service.sh <service> stop
.docker/service.sh <service> restart
```

- supervisorctl
```
.docker/service.sh supervisorctl status all
.docker/service.sh supervisorctl restart all
.docker/service.sh supervisorctl <name> <name>
```